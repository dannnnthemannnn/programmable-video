import 'dart:async';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:programmable_video_web/local_participant_listener.dart';
import 'package:programmable_video_web/programmable_video_web.dart';
import 'package:programmable_video_web/remote_participant_listener.dart';
import 'package:programmable_video_web/twilio_api.dart' as twilio_api;
import 'package:programmable_video_web/utils.dart';
import 'package:twilio_programmable_video_platform_interface/twilio_programmable_video_platform_interface.dart';
import 'package:universal_html/js.dart' as js;

class RoomListener {
  final twilio_api.Room? roomObj;
  final StreamController<BaseRoomEvent> roomStream = StreamController();
  final StreamController<BaseRemoteParticipantEvent> remoteParticipantStream = StreamController();
  final StreamController<BaseLocalParticipantEvent> localParticipantStream = StreamController();
  final StreamController<BaseRemoteDataTrackEvent> remoteDataTrackStream = StreamController();

  bool userDisconnected = false;

  LocalParticipantListener? _localParticipantListener;
  List<RemoteParticipantListener> _remoteParticipantListeners = [];

  RoomListener({this.roomObj});

  RoomModel get roomModel {
    return toRoomModel(roomObj!);
  }

  LocalParticipantListener? get localParticipantListener => _localParticipantListener;

  twilio_api.Participant get localParticipant => roomObj!.localParticipant;

  void initialize() {
    roomStream.add(Connected(roomModel));

    _localParticipantListener = LocalParticipantListener(
      roomListener: this,
      participant: roomObj!.localParticipant,
    )..initialize();

    print(roomObj!.participants);
    print(roomObj!.participants.runtimeType);
    print(roomObj!.participants.values);
    for (final participant in roomObj!.participants.values) {
      _remoteParticipantListeners.add(RemoteParticipantListener(
        roomListener: this,
        participant: participant,
      )..initialize());
    }

    roomObj!.once('disconnected', js.allowInterop((room, error) {
      _disconnectTracks();

      roomStream.add(Disconnected(toRoomModel(room), toTwilioException(error)));
    }));

    roomObj!.on('participantConnected', js.allowInterop((participant) {
      print('participant connected!');
      print(participant);
      roomStream.add(ParticipantConnected(roomModel, toRemoteParticipantModel(participant)));

      _remoteParticipantListeners.add(
          RemoteParticipantListener(roomListener: this, participant: participant)..initialize());
    }));

    roomObj!.on('participantDisconnected', js.allowInterop((participant) {
      final participantModel = toRemoteParticipantModel(participant);
      roomStream.add(ParticipantDisconnected(roomModel, participantModel));

      _remoteParticipantListeners
          .removeWhere((p) => p.remoteParticipantModel.sid == participantModel.sid);
    }));

    roomObj!.on('dominantSpeakerChanged', js.allowInterop((participant) {
      roomStream.add(DominantSpeakerChanged(
          roomModel, participant == null ? null : toRemoteParticipantModel(participant)));
    }));

    roomObj!.on('reconnecting', js.allowInterop((error) {
      // TODO: Investigate docs how to see connection failure
      roomStream.add(Reconnecting(roomModel, toTwilioException(error)));
    }));

    roomObj!.on('reconnected', js.allowInterop(() {
      roomStream.add(Reconnected(roomModel));
    }));

    roomObj!.on('recordingStarted', js.allowInterop(() {
      roomStream.add(RecordingStarted(roomModel));
    }));

    roomObj!.on('recordingStopped', js.allowInterop(() {
      roomStream.add(RecordingStopped(roomModel));
    }));
  }

  void disconnect() {
    userDisconnected = true;

    _disconnectTracks();

    print('disconnecting from room');
    roomObj!.disconnect();
  }

  twilio_api.Track? getVideoTrack(String? trackName) {
    final localParticipant = roomObj!.localParticipant;
    final localVideoTracks = roomObj!.localParticipant.videoTracks.values
        ?.where((t) => t.track != null)
        .map((t) => t.track)
        .toList();
    print('local video tracks: $localVideoTracks');
    final remoteParticipants = roomObj!.participants;
    print('remote participants: $remoteParticipants');
    final remoteVideoTracks = remoteParticipants.values
        ?.map((p) => p.videoTracks.values)
        .expand((tracks) => tracks)
        .where((t) => t.track != null)
        .map((t) => t.track)
        .toList();
    final allVideoTracks = [
      ...localVideoTracks!,
      if (remoteVideoTracks != null) ...remoteVideoTracks,
    ];
    print('searching for track name: $trackName');
    print('allTrack names: ${allVideoTracks.map((t) => t.name).toList()}');
    return allVideoTracks.firstWhereOrNull(
      (t) => t.name == trackName,
    );
  }

  void _disconnectTracks() {
    final List<twilio_api.TrackPublication> videoTracks =
        roomObj!.localParticipant.videoTracks.values;
    final List<twilio_api.TrackPublication> audioTracks =
        roomObj!.localParticipant.audioTracks.values;
    print('disabling video tracks');
    for (final videoTrackPub in videoTracks) {
      if (videoTrackPub.track != null) {
        final track = videoTrackPub.track;
        try {
          ProgrammableVideoWeb.setSrcNullForTrackElements(track.name);
          track.stop();
        } catch (e) {
          print('Error stopping video track: $e');
        }
      }
    }

    print('disabling audio tracks');
    for (final audioTrackPub in audioTracks) {
      if (audioTrackPub.track != null) {
        final track = audioTrackPub.track;
        try {
          track.stop();
        } catch (e) {
          print('Error stopping audio track: $e');
        }
      }
    }
  }

  String computeVideoTrackPriority() {
    final numParticipants = roomModel.remoteParticipants.length;

    return numParticipants < 9 ? 'standard' : 'low';
  }
}

RoomState? _parseRoomState(String state) {
  return RoomState.values
      .firstWhereOrNull((e) => e.toString().toLowerCase() == 'RoomState.$state'.toLowerCase());
}

Region? _parseRegion(String region) {
  return Region.values
      .firstWhereOrNull((e) => e.toString().toLowerCase() == 'Region.$region'.toLowerCase());
}

RoomModel toRoomModel(twilio_api.Room roomObj) {
  print(roomObj.participants);
  print(roomObj.participants.runtimeType);
  print(roomObj.participants.values);
  return RoomModel(
    sid: roomObj.sid,
    name: roomObj.name,
    isRecording: roomObj.isRecording,
    state: _parseRoomState(roomObj.state)!,
    mediaRegion: _parseRegion(roomObj.mediaRegion),
    localParticipant: toLocalParticipantModel(roomObj.localParticipant),
    remoteParticipants:
        roomObj.participants.values.map((p) => toRemoteParticipantModel(p)).toList(),
  );
}
