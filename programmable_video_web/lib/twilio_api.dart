@JS('Twilio.Video')
library twilioVideo;

import 'package:universal_html/html.dart' as html;

import 'package:js/js.dart';
import 'package:universal_html/js_util.dart' as js_util;

@JS('isSupported')
external bool isBrowserSupported();

@JS()
class Room {
  external String get sid;
  external String get name;
  external bool get isRecording;
  external String get state;
  external String get mediaRegion;

  external Participant get localParticipant;

  external JsMap<String, Participant> get participants;

  external void once(String event, Function callback);
  external void on(String event, Function callback);
  external void disconnect();
}

@JS()
class JsMap<T, S> {}

extension JsMapHelper<T, S> on JsMap<T, S> {
  List<S> get values {
    final participantList = js_util.callMethod(this, 'values', []);
    final arrayResults = js_util.callMethod(
        js_util.getProperty(html.window, 'Array'), 'from', [participantList]);

    return (arrayResults as List).cast<S>();
  }
}

@JS()
class Participant {
  external String get sid;
  external String get identity;
  external String get state;
  external String get signalingRegion;
  external int get networkQualityLevel;

  external JsMap<String, TrackPublication> get audioTracks;
  external JsMap<String, TrackPublication> get videoTracks;

  external void once(String event, Function callback);
  external void on(String event, Function callback);

  external dynamic publishTrack(Track track, options);
  external TrackPublication unpublishTrack(Track mediaTrack);
}

@JS()
class TrackPublication {
  external String get sid;
  external String get trackSid;
  external String get trackName;
  external bool get isSubscribed;
  external bool get isEnabled;

  external Track get track;

  external void unpublish();
}

@JS()
class Track {
  external String get sid;
  external String get name;
  external String get kind;

  external bool get isEnabled;
  external Dimenstions get dimensions;
  external bool get isSwitchedOff;
  external bool get isStarted;
  external bool get isStopped;
  external html.MediaStreamTrack get mediaStreamTrack;

  external void stop();

  external void enable();
  external void disable();

  external void attach(html.VideoElement? element);
  external void detach(html.VideoElement? element);

  external dynamic restart(options);
}

@JS()
class Dimenstions {
  external double get height;
  external double get width;
}
