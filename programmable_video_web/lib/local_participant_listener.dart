import 'dart:async';
import 'dart:ui';

import 'package:programmable_video_web/programmable_video_web.dart';
import 'package:programmable_video_web/room_listener.dart';
import 'package:programmable_video_web/twilio_api.dart' as twilio_api;
import 'package:programmable_video_web/utils.dart';
import 'package:twilio_programmable_video_platform_interface/twilio_programmable_video_platform_interface.dart';
import 'package:universal_html/js.dart' as js;

class LocalParticipantListener {
  final RoomListener? roomListener;
  final twilio_api.Participant? participant;

  LocalParticipantListener({this.roomListener, this.participant});

  StreamController<BaseLocalParticipantEvent> get stream => roomListener!.localParticipantStream;

  LocalParticipantModel get localParticipantModel => toLocalParticipantModel(participant!);

  void initialize() {
    print('registering local callbacks!');
    participant!.on('trackPublished', js.allowInterop((trackPublication) {
      final kind = trackPublication.kind;
      print('local track published: $kind');
      if (kind == 'audio') {
        stream.add(LocalAudioTrackPublished(
            localParticipantModel, toLocalAudioTrackPublicationModel(trackPublication)));
      } else if (kind == 'video') {
        stream.add(LocalVideoTrackPublished(
            localParticipantModel, toLocalVideoTrackPublicationModel(trackPublication)));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackPublicationFailed', js.allowInterop((error, localTrack) {
      final kind = localTrack.kind;
      if (kind == 'audio') {
        stream.add(LocalAudioTrackPublicationFailed(
          localAudioTrack: toLocalAudioTrack(localTrack),
          exception: toTwilioException(error),
          localParticipantModel: localParticipantModel,
        ));
      } else if (kind == 'video') {
        stream.add(LocalVideoTrackPublicationFailed(
          localVideoTrack: toLocalVideoTrack(localTrack),
          exception: toTwilioException(error),
          localParticipantModel: localParticipantModel,
        ));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackDimensionsChanged', js.allowInterop((videoTrack) {
      stream.add(
          LocalVideoTrackDimensionsChanged(localParticipantModel, toLocalVideoTrack(videoTrack)));
    }));

    participant!.on('networkQualityLevelChanged', js.allowInterop((level, stats) {
      stream.add(
          LocalNetworkQualityLevelChanged(localParticipantModel, parseNetworkQualityLevel(level)));
    }));

    participant!.on('trackStarted', js.allowInterop((track) {
      final kind = track.kind;
      if (kind == 'audio') {
        ProgrammableVideoWeb.registerAudioTrack(
          sid: 'local-$kind',
          track: track,
        );
        stream.add(LocalAudioTrackStarted(localParticipantModel, toLocalAudioTrack(track)));
      } else if (kind == 'video') {
        stream.add(LocalVideoTrackStarted(localParticipantModel, toLocalVideoTrack(track)));
      }
    }));

    participant!.on('trackStopped', js.allowInterop((track) {
      final kind = track.kind;
      if (kind == 'audio') {
        stream.add(LocalAudioTrackStopped(localParticipantModel, toLocalAudioTrack(track)));
      } else if (kind == 'video') {
        stream.add(LocalVideoTrackStopped(localParticipantModel, toLocalVideoTrack(track)));
      }
    }));
  }
}

LocalParticipantModel toLocalParticipantModel(twilio_api.Participant participant) {
  return LocalParticipantModel(
    identity: participant.identity,
    sid: participant.sid,
    state: participant.state,
    signalingRegion: participant.signalingRegion,
    networkQualityLevel: parseNetworkQualityLevel(participant.networkQualityLevel)!,
    localDataTrackPublications: [],
    localAudioTrackPublications: participant.audioTracks.values
        .map((pub) => toLocalAudioTrackPublicationModel(pub))
        .toList(),
    localVideoTrackPublications: participant.videoTracks.values
        .map((pub) => toLocalVideoTrackPublicationModel(pub))
        .toList(),
  );
}

LocalVideoTrackPublicationModel toLocalVideoTrackPublicationModel(
    twilio_api.TrackPublication publication) {
  final track = publication.track;

  return LocalVideoTrackPublicationModel(
    sid: publication.trackSid,
    localVideoTrack: toLocalVideoTrack(track),
  );
}

LocalAudioTrackPublicationModel toLocalAudioTrackPublicationModel(
    twilio_api.TrackPublication publication) {
  final track = publication.track;

  return LocalAudioTrackPublicationModel(
    sid: publication.trackSid,
    localAudioTrack: toLocalAudioTrack(track),
  );
}

LocalAudioTrackModel toLocalAudioTrack(twilio_api.Track track) {
  return LocalAudioTrackModel(
    name: track.name,
    enabled: track.isEnabled,
  );
}

LocalVideoTrackModel toLocalVideoTrack(twilio_api.Track track) {
  Size? dimensions;
  if (track.dimensions?.width != null && track.dimensions?.height != null) {
    dimensions = Size(track.dimensions!.width, track.dimensions!.height);
  }
  return LocalVideoTrackModel(
    name: track.name,
    enabled: track.isEnabled,
    isStarted: (track.isStarted ?? false) && !(track.isStopped ?? true),
    dimensions: dimensions,
    cameraCapturer: CameraCapturerModel(CameraSource.FRONT_CAMERA, 'CameraCapturer'),
  );
}
