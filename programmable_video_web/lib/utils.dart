import 'dart:async';

import 'package:twilio_programmable_video_platform_interface/twilio_programmable_video_platform_interface.dart';
import 'package:universal_html/html.dart' as html;
import 'package:universal_html/js_util.dart' as js_util;

Future<Object> promiseToFuture(Object promise) {
  return js_util.promiseToFuture(promise);
}

List<Object> mapToArray(Object object) {
  final participantList = js_util.callMethod(object, 'values', []);
  return js_util.callMethod(js_util.getProperty(html.window, 'Array'), 'from', [participantList]);
}

TwilioExceptionModel toTwilioException(Object error) {
  print(error);
  return TwilioExceptionModel(
      js_util.getProperty(error, 'code'), js_util.callMethod(error, 'toString', []));
}

NetworkQualityLevel? parseNetworkQualityLevel(Object level) {
  final lookup = {
    0: NetworkQualityLevel.NETWORK_QUALITY_LEVEL_ZERO,
    1: NetworkQualityLevel.NETWORK_QUALITY_LEVEL_ONE,
    2: NetworkQualityLevel.NETWORK_QUALITY_LEVEL_TWO,
    3: NetworkQualityLevel.NETWORK_QUALITY_LEVEL_THREE,
    4: NetworkQualityLevel.NETWORK_QUALITY_LEVEL_FOUR,
    5: NetworkQualityLevel.NETWORK_QUALITY_LEVEL_FIVE,
  };

  if (level is int && lookup.containsKey(level)) {
    return lookup[level];
  }

  return NetworkQualityLevel.NETWORK_QUALITY_LEVEL_UNKNOWN;
}

Object getPropertyChain(Object object, List<String> properties) {
  var result = object;
  for (final property in properties) {
    result = js_util.getProperty(result, property);
  }
  return result;
}
