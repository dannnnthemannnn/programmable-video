import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:programmable_video_web/local_participant_listener.dart';
import 'package:programmable_video_web/room_listener.dart';
import 'package:programmable_video_web/twilio_api.dart' as twilio_api;
import 'package:programmable_video_web/utils.dart';
import 'package:twilio_programmable_video_platform_interface/twilio_programmable_video_platform_interface.dart';
import 'package:universal_html/html.dart' as html;
import 'package:universal_html/js.dart' as js;
import 'package:universal_html/js_util.dart' as js_util;

class ProgrammableVideoWeb extends ProgrammableVideoPlatform {
  RoomListener? _roomListener;

  static Map<String?, List<html.VideoElement?>> videoViewLookup = {};
  static Map<String?, twilio_api.Track?> _audioTrackLookup = {};

  static bool get isMobile {
    final agent = html.window.navigator.userAgent.toLowerCase();
    return ['ipad', 'iphone', 'ipod', 'android'].any((s) => agent.contains(s));
  }

  static void registerAudioTrack({String? sid, Object? track}) {
    print('setting up audio track: $sid');
    _audioTrackLookup[sid] = track as twilio_api.Track?;
  }

  static void registerWith(Registrar registrar) {
    ProgrammableVideoPlatform.instance = ProgrammableVideoWeb();
  }

  @override
  Future<void> disconnect() async {
    _roomListener?.disconnect();
  }

  @override
  Future<int> connectToRoom(ConnectOptionsModel connectOptions) async {
    final videoConstraints = computeVideoTrackConstraints();
    if (connectOptions.defaultVideoDeviceId != null) {
      videoConstraints['deviceId'] = connectOptions.defaultVideoDeviceId;
    }
    dynamic audioConstraints = connectOptions.enableAudio ?? false;
    if (connectOptions.defaultAudioDeviceId != null) {
      audioConstraints = {'deviceId': connectOptions.defaultAudioDeviceId};
    }

    print('connecting to room ');
    twilio_api.Room roomObj =
        await js_util.promiseToFuture(js_util.callMethod(_twilioVideoObject(), 'connect', [
      connectOptions.accessToken,
      js_util.jsify(
        {
          'dominantSpeaker': true,
          'preferredAudioCodecs':
              (connectOptions.preferredAudioCodecs ?? []).map((c) => c.name).toList(),
          'preferredVideoCodecs': [
            {
              'codec': 'VP8',
              'simulcast': true,
            }
          ],
          'maxAudioBitrate': '16000',
          'bandwidthProfile': {
            'video': {
              if (isMobile) 'maxSubscriptionBitrate': 2500000,
              'mode': 'grid',
              'dominantSpeakerPriority': 'standard',
              'trackSwitchOffMode': 'detected',
            },
          },
          'video': (connectOptions.enableVideo ?? false) ? videoConstraints : false,
          'audio': audioConstraints,
          // Can be updated to give more detailed statistics about the local participant.
          // See https://www.twilio.com/docs/video/using-network-quality-api
          'networkQuality': true,
        },
      )
    ]) as Object);

    print('done connecting');

    // TODO: Clean up old room
    _roomListener = RoomListener(roomObj: roomObj)..initialize();

    return 1;
  }

  Map<dynamic, dynamic> computeVideoTrackConstraints() {
    return {
      'width': {'ideal': 720},
      'height': {'ideal': 480},
      'frameRate': {'ideal': 24},
    };
  }

  @override
  void updateVideoTrack({String? name, html.VideoElement? element, bool? attach}) {
    final track = _roomListener!.getVideoTrack(name);
    if (track == null) {
      print('UpdateVideoTrack: Track with name $name not found! Returning early');
      return;
    }

    if (attach!) {
      print('attaching to $name');
      track.attach(element);
      videoViewLookup[name] ??= [];
      videoViewLookup[name]!.add(element);
    } else {
      print('detaching from $name');
      element?.srcObject = null;
      track.detach(element);
      element?.remove();
    }
  }

  Future<void> _publishLocalTracks({
    String? audioDeviceId,
    String? videoDeviceId,
    bool audio = false,
    bool video = false,
  }) async {
    List<twilio_api.Track> localTracks = [];
    if (audio && video) {
      localTracks = await js_util
          .promiseToFuture(js_util.callMethod(_twilioVideoObject(), 'createLocalTracks', [
        js_util.jsify({
          'video': computeVideoTrackConstraints(),
          'audio': true,
        })
      ]));
    } else if (video) {
      print("Creating local video track!");
      final track = await js_util
          .promiseToFuture(js_util.callMethod(_twilioVideoObject(), 'createLocalVideoTrack', [
        js_util.jsify({
          ...computeVideoTrackConstraints(),
          if (videoDeviceId != null) 'deviceId': {'ideal': videoDeviceId},
        })
      ]));
      localTracks = [track];
    } else if (audio) {
      final track = await js_util
          .promiseToFuture(js_util.callMethod(_twilioVideoObject(), 'createLocalAudioTrack', [
        js_util.jsify({
          if (audioDeviceId != null) 'deviceId': audioDeviceId,
        })
      ]));
      localTracks = [track];
    }

    // Verify the user has not disconnected.
    final isValidTracks = localTracks.map(_verifyNewTrackIsValid).every((valid) => valid);

    if (isValidTracks) {
      for (final track in localTracks) {
        await js_util.promiseToFuture(_roomListener!.localParticipant.publishTrack(
          track,
          js_util.jsify({
            if (track.kind == 'video') 'priority': _roomListener!.computeVideoTrackPriority(),
          }),
        ));
      }
    }
  }

  // If the user has disconnected, this stops the track.
  bool _verifyNewTrackIsValid(twilio_api.Track track) {
    if (_roomListener!.userDisconnected) {
      // Handle tracks staying on after disconnect
      try {
        if (track.kind == 'video') {
          setSrcNullForTrackElements(track.name);
        }
        track.stop();
      } catch (e) {
        print('Error stopping track: $e');
      }
      return false;
    }

    return true;
  }

  static setSrcNullForTrackElements(String trackName) {
    print('setting null for track $trackName');
    print('found videos: ${videoViewLookup[trackName]}');
    for (final track in videoViewLookup[trackName] ?? []) {
      track?.srcObject = null;
    }
  }

  bool _currentTrackMatchesDevice(
      html.MediaDeviceInfo? device, twilio_api.TrackPublication? audioTrackPub) {
    if (audioTrackPub?.track == null) return false;

    if (device == null) return true;

    final Map<dynamic, dynamic> settings =
        audioTrackPub?.track?.mediaStreamTrack?.getSettings() ?? new Map();
    return settings['deviceId'] == device?.deviceId && settings['groupId'] == device?.groupId;
  }

  @override
  Future<bool> enableVideoTrack({
    bool? enable,
    String? name,
    String? deviceId,
  }) async {
    print('setting enabled == $enable video track: $name deviceId = $deviceId');

    final videoTrackPub = _roomListener!.roomObj!.localParticipant.videoTracks.values
        .firstWhereOrNull((trackPub) => trackPub.trackName == name);
    print('local track: $videoTrackPub');

    if (enable! && videoTrackPub?.track == null) {
      // Create new video track for user
      await _publishLocalTracks(
        videoDeviceId: deviceId,
        video: true,
      );
    } else if (enable) {
      // Restart existing track with new deviceId
      final track = videoTrackPub!.track;
      setSrcNullForTrackElements(track.name);
      await js_util.promiseToFuture(track.restart(js_util.jsify({
        ...computeVideoTrackConstraints(),
        if (deviceId != null) 'deviceId': deviceId,
      })));
      if (!_verifyNewTrackIsValid(track)) {
        return false;
      }
      track.enable();
    } else {
      // End any publications that are not named here.
      _endTrackPublications(_roomListener!.roomObj!.localParticipant.videoTracks.values
          .where((t) => t?.trackName != name && t?.trackName != 'screen-share'));
      if (videoTrackPub?.track != null) {
        setSrcNullForTrackElements(videoTrackPub!.track.name);
        videoTrackPub.track.stop();
        videoTrackPub.track.disable();
      }
    }

    return true;
  }

  void _endTrackPublications(Iterable<twilio_api.TrackPublication> tracks) {
    for (final audioTrackPub in tracks) {
      audioTrackPub.track?.stop();
      audioTrackPub.unpublish();
    }
  }

  Future<bool> enableAudioTrack({
    bool? enable,
    String? name,
    html.MediaDeviceInfo? device,
  }) async {
    final deviceId = device?.deviceId;
    print('enable: $enable audio track: $name with deviceId: $deviceId');

    final audioTrackPub = _roomListener!.localParticipant.audioTracks.values
        .firstWhereOrNull((trackPub) => trackPub.trackName == name);

    if (_currentTrackMatchesDevice(device, audioTrackPub)) {
      print('Current audio track ${audioTrackPub} matches deviceId: ${device?.deviceId}');

      // End any other audio track publications that were made.
      _endTrackPublications(
          _roomListener!.localParticipant.audioTracks.values.where((tp) => tp.trackName != name));

      if (enable!) {
        audioTrackPub!.track.enable();
      } else {
        audioTrackPub!.track.disable();
      }
    } else {
      print('Current track does not match, ending all current audio tracks');
      _endTrackPublications(_roomListener!.localParticipant.audioTracks.values);

      if (enable!) {
        print('enabling new track with device ID $deviceId');
        // Create new audio track for user
        await _publishLocalTracks(
          audioDeviceId: deviceId,
          audio: true,
        );
      }
    }

    return true;
  }

  /// TODO: Specify device IDs
  Future<void> enableAudioAndVideoTrack() async {
    // Create new video track for user
    await _publishLocalTracks(
      video: true,
      audio: true,
    );
  }

  Future<void> enableRemoteAudioTrack({bool? enable, String? sid}) async {
    enableAudioTrackInternal(enable: enable, sid: sid);
  }

  void enableAudioTrackInternal({bool? enable, String? sid}) {
    print('enabling audio track: $sid');
    final audioTrack = _audioTrackLookup[sid];
    if (audioTrack == null) {
      print('audio track not found');
      return;
    }

    if (enable!) {
      print('attaching: $sid');
      html.document.body!.append(js_util.callMethod(audioTrack, 'attach', []));
      print('attached: $sid');
    } else {
      final mediaElements = js_util.callMethod(audioTrack, 'detach', []);
      mediaElements.forEach((element) => (element as html.MediaElement).remove());
    }
  }

  @override
  html.MediaStreamTrack getAudioTrack({String? sid, String? trackName}) {
    final isLocal =
        _roomListener?.localParticipant != null && sid == _roomListener?.localParticipant?.sid;

    var participant = isLocal
        ? _roomListener!.localParticipant
        : _roomListener!.roomObj!.participants.values.firstWhereOrNull((p) => p.sid == sid);

    print('getting local audio track with participant sid: $sid, name: $trackName');

    return participant!.audioTracks.values
        .firstWhereOrNull((trackPub) => trackPub.trackName == trackName)!
        .track!
        .mediaStreamTrack;
  }

  Future<void> startScreenShare() async {
    final mediaTrack = await js_util
        .promiseToFuture(js_util.callMethod(html.window, 'getScreenShareForLocalParticipant', []));

    await js_util.promiseToFuture(_roomListener!.localParticipant.publishTrack(
        mediaTrack,
        js_util.jsify({
          'priority': 'high',
        })));

    final mediaStreamTrack = mediaTrack.mediaStreamTrack;
    js_util.setProperty(mediaStreamTrack, 'onended', js.allowInterop((event) {
      print('track ended');
      _unpublishScreenShare(mediaTrack);
    }));
  }

  @override
  Future<void> stopScreenShare() async {
    final screenShareTrackPub = _roomListener!.localParticipant.videoTracks.values
        .firstWhereOrNull((trackPub) => trackPub.trackName == 'screen-share');
    if (screenShareTrackPub?.track != null) {
      print('stopping track');
      screenShareTrackPub!.track.stop();
      _unpublishScreenShare(screenShareTrackPub.track);
    } else {
      print('screen share track not found!');
    }
  }

  void _unpublishScreenShare(twilio_api.Track mediaTrack) {
    final unpublishedTrack = _roomListener!.localParticipant.unpublishTrack(mediaTrack);
    _roomListener!.localParticipantStream.add(LocalVideoTrackUnpublished(
        _roomListener!.localParticipantListener!.localParticipantModel,
        toLocalVideoTrackPublicationModel(unpublishedTrack)));
  }

  /// Calls native code to see if this client is supported.
  @override
  Future<bool> isSupported() async {
    final isSupported = getPropertyChain(html.window, ['Twilio', 'Video', 'isSupported']);

    return isSupported as bool;
  }

  Object _twilioVideoObject() {
    return getPropertyChain(html.window, ['Twilio', 'Video']);
  }

  @override
  Future<void> setNativeDebug(bool native) {
    return Future.sync(() => null);
  }

  /// Stream of the CameraEvent model.
  ///
  /// This stream is used to listen for async events after interactions with the camera.
  Stream<BaseCameraEvent> cameraStream() {
    return Stream.value(SkipableCameraEvent());
  }

  /// Stream of the BaseRoomEvent model.
  ///
  /// This stream is used to update the Room in a plugin implementation.
  Stream<BaseRoomEvent> roomStream(int internalId) {
    return _roomListener!.roomStream.stream;
  }

  /// Stream of the BaseRemoteParticipantEvent model.
  ///
  /// This stream is used to update the RemoteParticipants in a plugin implementation.
  Stream<BaseRemoteParticipantEvent> remoteParticipantStream(int internalId) {
    return _roomListener!.remoteParticipantStream.stream;
  }

  /// Stream of the BaseLocalParticipantEvent model.
  ///
  /// This stream is used to update the LocalParticipant in a plugin implementation.
  Stream<BaseLocalParticipantEvent> localParticipantStream(int internalId) {
    return _roomListener!.localParticipantStream.stream;
  }

  /// Stream of the BaseRemoteDataTrackEvent model.
  ///
  /// This stream is used to update the RemoteDataTrack in a plugin implementation.
  Stream<BaseRemoteDataTrackEvent> remoteDataTrackStream(int internalId) {
    return _roomListener!.remoteDataTrackStream.stream;
  }

  /// Stream of dynamic that contains all the native logging output.
  Stream<dynamic> loggingStream() {
    return Stream.value('');
  }
//#endregion
}
