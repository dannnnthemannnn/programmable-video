import 'dart:async';
import 'dart:ui';

import 'package:programmable_video_web/programmable_video_web.dart';
import 'package:programmable_video_web/room_listener.dart';
import 'package:programmable_video_web/twilio_api.dart' as twilio_api;
import 'package:programmable_video_web/utils.dart';
import 'package:twilio_programmable_video_platform_interface/twilio_programmable_video_platform_interface.dart';
import 'package:universal_html/js.dart' as js;

class RemoteParticipantListener {
  final RoomListener? roomListener;
  final twilio_api.Participant? participant;

  RemoteParticipantListener({this.roomListener, this.participant});

  StreamController<BaseRemoteParticipantEvent> get stream => roomListener!.remoteParticipantStream;

  RemoteParticipantModel get remoteParticipantModel => toRemoteParticipantModel(participant!);

  void initialize() {
    participant!.on('trackDisabled', js.allowInterop((trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        stream.add(RemoteAudioTrackDisabled(
            remoteParticipantModel, toRemoteAudioTrackPublicationModel(trackPublication)));
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackPublished(
            remoteParticipantModel, toRemoteVideoTrackPublicationModel(trackPublication)));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    // TODO: Type these interop methods
    participant!.on('trackEnabled', js.allowInterop((trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        stream.add(RemoteAudioTrackEnabled(
            remoteParticipantModel, toRemoteAudioTrackPublicationModel(trackPublication)));
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackEnabled(
            remoteParticipantModel, toRemoteVideoTrackPublicationModel(trackPublication)));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackPublished', js.allowInterop((trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        final publicationModel = toRemoteAudioTrackPublicationModel(trackPublication);
        stream.add(RemoteAudioTrackPublished(remoteParticipantModel, publicationModel));
      } else if (kind == 'video') {
        final publicationModel = toRemoteVideoTrackPublicationModel(trackPublication);
        stream.add(RemoteVideoTrackPublished(remoteParticipantModel, publicationModel));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackUnpublished', js.allowInterop((trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        stream.add(RemoteAudioTrackUnpublished(
            remoteParticipantModel, toRemoteAudioTrackPublicationModel(trackPublication)));
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackUnpublished(
            remoteParticipantModel, toRemoteVideoTrackPublicationModel(trackPublication)));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackSubscribed', js.allowInterop((track, trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        stream.add(RemoteAudioTrackSubscribed(
          remoteParticipantModel: remoteParticipantModel,
          remoteAudioTrackPublicationModel: toRemoteAudioTrackPublicationModel(trackPublication),
          remoteAudioTrackModel: toRemoteAudioTrack(track),
        ));
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackSubscribed(
          remoteParticipantModel: remoteParticipantModel,
          remoteVideoTrackPublicationModel: toRemoteVideoTrackPublicationModel(trackPublication),
          remoteVideoTrackModel: toRemoteVideoTrack(track),
        ));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackUnsubscribed', js.allowInterop((track, trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        stream.add(RemoteAudioTrackUnsubscribed(
          remoteParticipantModel: remoteParticipantModel,
          remoteAudioTrackPublicationModel: toRemoteAudioTrackPublicationModel(trackPublication),
          remoteAudioTrackModel: toRemoteAudioTrack(track),
        ));

        ProgrammableVideoPlatform.instance.enableRemoteAudioTrack(enable: false, sid: track.sid);
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackUnsubscribed(
          remoteParticipantModel: remoteParticipantModel,
          remoteVideoTrackPublicationModel: toRemoteVideoTrackPublicationModel(trackPublication),
          remoteVideoTrackModel: toRemoteVideoTrack(track),
        ));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackSubscriptionFailed', js.allowInterop((error, trackPublication) {
      final kind = trackPublication.kind;
      if (kind == 'audio') {
        stream.add(RemoteAudioTrackSubscriptionFailed(
          remoteParticipantModel: remoteParticipantModel,
          remoteAudioTrackPublicationModel: toRemoteAudioTrackPublicationModel(trackPublication),
          exception: toTwilioException(error),
        ));
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackSubscriptionFailed(
          remoteParticipantModel: remoteParticipantModel,
          remoteVideoTrackPublicationModel: toRemoteVideoTrackPublicationModel(trackPublication),
          exception: toTwilioException(error),
        ));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackDimensionsChanged', js.allowInterop((videoTrack) {
      stream.add(RemoteVideoTrackDimensionsChanged(
          remoteParticipantModel, toRemoteVideoTrack(videoTrack)));
    }));

    participant!.on('trackStarted', js.allowInterop((track) {
      final kind = track.kind;
      if (kind == 'audio') {
        ProgrammableVideoWeb.registerAudioTrack(
          sid: track.sid,
          track: track,
        );
        ProgrammableVideoPlatform.instance.enableRemoteAudioTrack(
          enable: true,
          sid: track.sid,
        );
      } else if (kind == 'video') {
        stream.add(RemoteVideoTrackStarted(remoteParticipantModel, toRemoteVideoTrack(track)));
      } else if (kind == 'data') {
        throw UnimplementedError();
      }
    }));

    participant!.on('trackSwitchedOn', js.allowInterop((videoTrack, _) {
      stream
          .add(RemoteVideoTrackSwitchedOn(remoteParticipantModel, toRemoteVideoTrack(videoTrack)));
    }));

    participant!.on('trackSwitchedOff', js.allowInterop((videoTrack, _) {
      stream
          .add(RemoteVideoTrackSwitchedOff(remoteParticipantModel, toRemoteVideoTrack(videoTrack)));
    }));

    participant!.on('disconnected', js.allowInterop((participant) {
      stream.add(RemoteDisconnected(
        remoteParticipantModel,
      ));
    }));

    participant!.on('reconnecting', js.allowInterop((participant) {
      stream.add(RemoteReconnecting(
        remoteParticipantModel,
      ));
    }));

    participant!.on('reconnected', js.allowInterop((participant) {
      stream.add(RemoteReconnected(
        remoteParticipantModel,
      ));
    }));
  }
}

RemoteParticipantModel toRemoteParticipantModel(twilio_api.Participant participant) {
  return RemoteParticipantModel(
    identity: participant.identity,
    sid: participant.sid,
    state: participant.state,
    networkQualityLevel: parseNetworkQualityLevel(participant.networkQualityLevel)!,
    remoteDataTrackPublications: [],
    remoteAudioTrackPublications: participant.audioTracks.values
        .map((pub) => toRemoteAudioTrackPublicationModel(pub))
        .toList(),
    remoteVideoTrackPublications: participant.videoTracks.values
        .map((pub) => toRemoteVideoTrackPublicationModel(pub))
        .toList(),
  );
}

RemoteVideoTrackPublicationModel toRemoteVideoTrackPublicationModel(
    twilio_api.TrackPublication publication) {
  final track = publication.track;

  final model = RemoteVideoTrackPublicationModel(
    sid: publication.trackSid,
    name: publication.trackName,
    subscribed: publication.isSubscribed,
    enabled: publication.isEnabled,
    remoteVideoTrack: toRemoteVideoTrack(track),
  );

  return model;
}

RemoteAudioTrackPublicationModel toRemoteAudioTrackPublicationModel(
    twilio_api.TrackPublication publication) {
  final track = publication.track;

  return RemoteAudioTrackPublicationModel(
    sid: publication.trackSid,
    name: publication.trackName,
    subscribed: publication.isSubscribed,
    enabled: publication.isEnabled,
    remoteAudioTrack: toRemoteAudioTrack(track),
  );
}

RemoteAudioTrackModel toRemoteAudioTrack(twilio_api.Track track) {
  return RemoteAudioTrackModel(
    name: track.name,
    enabled: track.isEnabled,
    sid: track.sid,
  );
}

RemoteVideoTrackModel toRemoteVideoTrack(twilio_api.Track track) {
  Size? dimensions;
  if (track.dimensions?.width != null && track.dimensions?.height != null) {
    dimensions = Size(track.dimensions.width, track.dimensions.height);
  }
  return RemoteVideoTrackModel(
    name: track.name,
    enabled: track.isEnabled,
    sid: track.sid,
    isSwitchedOff: track.isSwitchedOff,
    isStarted: track.isStarted,
    dimensions: dimensions,
  );
}
