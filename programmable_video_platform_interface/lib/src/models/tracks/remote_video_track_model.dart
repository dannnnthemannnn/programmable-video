import 'dart:ui';

import 'package:twilio_programmable_video_platform_interface/src/models/model_exports.dart';

/// Model that a plugin implementation can use to construct a RemoteVideoTrack.
class RemoteVideoTrackModel extends TrackModel {
  final String sid;
  final Size? dimensions;
  final bool isSwitchedOff;
  final bool isStarted;

  const RemoteVideoTrackModel({
    required String name,
    required bool enabled,
    required this.dimensions,
    required this.sid,
    required this.isSwitchedOff,
    required this.isStarted,
  })  : assert(name != null),
        assert(enabled != null),
        assert(sid != null),
        assert(isSwitchedOff != null),
        super(
          name: name,
          enabled: enabled,
        );

  factory RemoteVideoTrackModel.fromEventChannelMap(Map<String, dynamic> map) {
    return RemoteVideoTrackModel(
        name: map['name'],
        enabled: map['enabled'],
        dimensions: map['dimensions'],
        sid: map['sid'],
        isSwitchedOff: false,
        isStarted: false);
  }

  @override
  String toString() {
    return '{ name: $name, enabled: $enabled, sid: $sid, isSwitchedOff: $isSwitchedOff, isStarted: $isStarted, dimensions: $dimensions }';
  }

  @override

  /// Create map from properties.
  Map<String, Object?> toMap() {
    return <String, Object?>{
      'enable': enabled,
      'name': name,
      'dimensions': dimensions,
    };
  }
}
