part of twilio_programmable_video;

abstract class VideoTrack extends Track {
  Size? _dimensions;
  bool? _isStarted;

  VideoTrack(enabled, name, dimensions, isStarted)
      : _dimensions = dimensions,
        _isStarted = isStarted,
        assert(enabled != null),
        assert(name != null),
        super(enabled, name);

  Size? get dimensions => _dimensions;
  bool? get isStarted => _isStarted;

  /// Update properties from a [TrackModel].
  @override
  void _updateFromModel(TrackModel model) {
    if (model is RemoteVideoTrackModel) {
      _isStarted = model.isStarted;
      _dimensions = model.dimensions;
    } else if (model is LocalVideoTrackModel) {
      _isStarted = model.isStarted;
      _dimensions = model.dimensions;
    }
    super._updateFromModel(model);
  }
}
