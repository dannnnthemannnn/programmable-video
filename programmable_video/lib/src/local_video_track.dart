part of twilio_programmable_video;

/// A local video track that gets video frames from a specified [VideoCapturer].
class LocalVideoTrack extends VideoTrack {
  Widget? _widget;

  final VideoCapturer _videoCapturer;

  /// Check if it is enabled.
  ///
  /// When the value is `false`, blank video frames are sent. When the value is `true`, frames from the [CameraSource] are provided.
  @override
  bool? get isEnabled => super._enabled;

  /// Retrieves the [VideoCapturer].
  VideoCapturer get videoCapturer => _videoCapturer;

  LocalVideoTrack(
    enabled,
    this._videoCapturer, {
    String name = '',
    Size? dimensions,
    bool? isStarted,
  })  : assert(_videoCapturer != null),
        super(enabled, name, dimensions, isStarted);

  /// Construct from a [LocalVideoTrackModel].
  factory LocalVideoTrack._fromModel(LocalVideoTrackModel model) {
    var videoCapturer = model.cameraCapturer.type == 'CameraCapturer'
        ? CameraCapturer._fromModel(model.cameraCapturer)
        : throw Exception('Received unknown VideoCapturer');
    var localVideoTrack = LocalVideoTrack(
      model.enabled,
      videoCapturer,
      name: model.name!,
      dimensions: model.dimensions,
      isStarted: model.isStarted,
    );
    localVideoTrack._updateFromModel(model);
    return localVideoTrack;
  }

  /// Dispose the videoCapturer
  void _dispose() {
    videoCapturer._dispose();
  }

  /// Returns a native widget.
  ///
  /// By default the widget will be mirrored, to change that set [mirror] to false.
  /// If you provide a [key] make sure it is unique among all [VideoTrack]s otherwise Flutter might send the wrong creation params to the native side.
  Widget widget({bool mirror = true, Key? key}) {
    key ??= ValueKey(name);

    var creationParams = {
      'isLocal': true,
      'mirror': mirror,
    };

    return _widget ??= VideoTrackViewWeb(name: name);

    if (false) {
      return _widget ??= AndroidView(
        key: key,
        viewType: 'twilio_programmable_video/views',
        creationParams: creationParams,
        creationParamsCodec: const StandardMessageCodec(),
        onPlatformViewCreated: (int viewId) {
          TwilioProgrammableVideo._log(
              'LocalVideoTrack => View created: $viewId, creationParams: $creationParams');
        },
      );
    }

    if (false) {
      return _widget ??= UiKitView(
        key: key,
        viewType: 'twilio_programmable_video/views',
        creationParams: creationParams,
        creationParamsCodec: const StandardMessageCodec(),
        onPlatformViewCreated: (int viewId) {
          TwilioProgrammableVideo._log(
              'LocalVideoTrack => View created: $viewId, creationParams: $creationParams');
        },
      );
    }

    throw Exception('No widget implementation found for platform ');
  }

  @override
  void _updateFromModel(TrackModel track) {
    _dimensions = (track as LocalVideoTrackModel).dimensions;
    _isStarted = track.isStarted;
    super._updateFromModel(track);
  }

  /// Create [LocalVideoTrackModel] from properties.
  TrackModel _toModel() {
    final cameraCapturer = videoCapturer as CameraCapturer;
    return LocalVideoTrackModel(
      enabled: _enabled!,
      name: _name!,
      cameraCapturer: CameraCapturerModel(cameraCapturer.cameraSource!, 'CameraCapturer'),
      dimensions: Size.zero,
      isStarted: false,
    );
  }
}
