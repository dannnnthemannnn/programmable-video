import 'dart:html' as html;
import 'dart:math' as math;
import 'dart:ui_web' as ui_web;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:twilio_programmable_video_platform_interface/twilio_programmable_video_platform_interface.dart';

class VideoTrackViewWeb extends StatefulWidget {
  final String? name;
  const VideoTrackViewWeb({this.name});

  @override
  _VideoTrackViewWebState createState() => _VideoTrackViewWebState();
}

class _VideoTrackViewWebState extends State<VideoTrackViewWeb> {
  // TODO: Replace with a uuid
  final _viewId = math.Random().nextInt(99999999).toString();
  String get _platformViewId => 'twilio_programmable_video/views/$_viewId';

  html.VideoElement? _videoElement;

  void _updateVideoElement({bool? attach}) {
    ProgrammableVideoPlatform.instance
        .updateVideoTrack(name: widget.name, element: _videoElement, attach: attach);
  }

  @override
  void initState() {
    ui_web.platformViewRegistry
        .registerViewFactory('twilio_programmable_video/views/$_platformViewId', (_) {
      _videoElement = html.VideoElement()
        ..id = 'junto-test-$_platformViewId'
        ..style.width = '100%'
        ..style.height = '100%'
        ..muted = true
        ..autoplay = true;

      print(_videoElement!.style.width);
      print(_videoElement!.style.height);

      _updateVideoElement(attach: true);
      print(_videoElement!.style.width);
      print(_videoElement!.style.height);

      return _videoElement;
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    print('dispose in widget');
  }

  @override
  Widget build(BuildContext context) {
    return VideoVideoHtmlElementView(
      viewType: 'twilio_programmable_video/views/$_platformViewId',
      onDispose: () {
        print('disposing');
        _updateVideoElement(attach: false);
      },
    );
  }
}

class VideoVideoHtmlElementView extends StatelessWidget {
  /// Creates a platform view for Flutter Web.
  ///
  /// `viewType` identifies the type of platform view to create.
  const VideoVideoHtmlElementView({
    Key? key,
    required this.viewType,
    this.onDispose,
  })  : assert(viewType != null),
        assert(kIsWeb, 'VideoHtmlElementView is only available on Flutter Web.'),
        super(key: key);

  /// The unique identifier for the HTML view type to be embedded by this widget.
  ///
  /// A PlatformViewFactory for this type must have been registered.
  final String viewType;

  final void Function()? onDispose;

  @override
  Widget build(BuildContext context) {
    return PlatformViewLink(
      viewType: viewType,
      onCreatePlatformView: _createVideoHtmlElementView,
      surfaceFactory: (BuildContext context, PlatformViewController controller) {
        return PlatformViewSurface(
          controller: controller,
          gestureRecognizers: const <Factory<OneSequenceGestureRecognizer>>{},
          hitTestBehavior: PlatformViewHitTestBehavior.opaque,
        );
      },
    );
  }

  /// Creates the controller and kicks off its initialization.
  _VideoHtmlElementViewController _createVideoHtmlElementView(PlatformViewCreationParams params) {
    final _VideoHtmlElementViewController controller =
        _VideoHtmlElementViewController(params.id, viewType, onDispose);
    controller._initialize().then((_) {
      params.onPlatformViewCreated(params.id);
    });
    return controller;
  }
}

class _VideoHtmlElementViewController extends PlatformViewController {
  _VideoHtmlElementViewController(
    this.viewId,
    this.viewType,
    this.onDispose,
  );

  @override
  final int viewId;

  /// The unique identifier for the HTML view type to be embedded by this widget.
  ///
  /// A PlatformViewFactory for this type must have been registered.
  final String viewType;

  final void Function()? onDispose;

  bool _initialized = false;

  Future<void> _initialize() async {
    final Map<String, dynamic> args = <String, dynamic>{
      'id': viewId,
      'viewType': viewType,
    };
    await SystemChannels.platform_views.invokeMethod<void>('create', args);
    _initialized = true;
  }

  @override
  Future<void> clearFocus() {
    // Currently this does nothing on Flutter Web.
    // TODO(het): Implement this. See https://github.com/flutter/flutter/issues/39496
    return Future.sync(() => null);
  }

  @override
  Future<void> dispatchPointerEvent(PointerEvent event) {
    // We do not dispatch pointer events to HTML views because they may contain
    // cross-origin iframes, which only accept user-generated events.
    return Future.sync(() => null);
  }

  @override
  Future<void> dispose() {
    if (_initialized) {
      onDispose!();
      // Asynchronously dispose this view.
      SystemChannels.platform_views.invokeMethod<void>('dispose', viewId);
    }

    return Future.sync(() => null);
  }
}
