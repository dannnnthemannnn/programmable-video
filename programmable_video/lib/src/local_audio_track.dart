part of twilio_programmable_video;

/// Represents a local audio source.
class LocalAudioTrack extends AudioTrack {
  @override
  bool? _enabled;

  /// Check if it is enabled.
  ///
  /// When the value is `false`, the local audio track is muted. When the value is `true` the local audio track is live.
  @override
  bool? get isEnabled => _enabled;

  LocalAudioTrack(bool this._enabled, {String name = ''}) : super(_enabled, name);

  /// Construct from a [LocalAudioTrackModel].
  factory LocalAudioTrack._fromModel(LocalAudioTrackModel model) {
    var localAudioTrack = LocalAudioTrack(model.enabled!, name: model.name!);
    localAudioTrack._updateFromModel(model);
    return localAudioTrack;
  }

  /// Create [TrackModel] from properties.
  TrackModel _toModel() {
    return LocalAudioTrackModel(
      enabled: _enabled!,
      name: _name!,
    );
  }
}
